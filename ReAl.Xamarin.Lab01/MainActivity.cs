﻿using System;
using Android.App;
using Android.Widget;
using Android.OS;
using Android.Provider;

namespace ReAl.Xamarin.Lab01
{
    [Activity(Label = "ReAl.Xamarin.Lab01", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        private Button _button;
        private TextView _txtViewDev;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
            _button = FindViewById<Button>(Resource.Id.MyButton);
            _txtViewDev = FindViewById<TextView>(Resource.Id.textView1);

            _button.Click += ButtonOnClick;
        }

        private async void ButtonOnClick(object sender, EventArgs eventArgs)
        {
            _txtViewDev.Text = "Reynaldo Alonzo Vera Arias";
            var miCorreo = "7.re.al.7@gmail.com";
            var miDeviceId = Settings.Secure.GetString(ContentResolver, Settings.Secure.AndroidId);

            var miServicio = new ServiceHelper();
            await miServicio.InsertarEntidad(miCorreo, "Lab1", miDeviceId);

            _button.Text = "Gracias por Registrarte";
        }
    }
}

